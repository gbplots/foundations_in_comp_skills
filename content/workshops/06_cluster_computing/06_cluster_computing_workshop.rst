
Workshop 6. Shell Scripts and Cluster Computing 
===============================================

.. contents::
    :local:

Boston University Shared Computing Cluster (SCC) 
++++++++++++++++++++++++++++++++++++++++++++++++++++++
The SCC is a high performance computing resource available to BU researchers. The SCC is a Linux-based system with over 11 thousand processors, and it is located in Holyoke, MA.    

This document provides details specific to BU SCC, and it is an addition to the `Shell Scripts and Cluster Computing <06_cluster_computing.html>`_ material. This document provides a very quick introduction to BU SCC, meant to get new students started right away. For more detailed information check out the `SCC Quick Start Guide <http://www.bu.edu/tech/support/research/system-usage/scc-quickstart/>`_ and the more detailed `Using the System <http://www.bu.edu/tech/support/research/system-usage/using-scc/>`_ section of the documentation. 



Connecting to SCC
++++++++++++++++++++++++++++++++++++++++++++++++++++++
To be able to connect to SCC, you need to be added to a project active on SCC. Eventually, your PI or collaborator will add you to the appropriate project, but for the purpose of this workshop, you've been added temporarily to the BU Bioinformatics Hub SCC project.   

To connect to SCC, you need to log in with an SSH client, using your BU Kerberos user name and password.   
Bioinformatics researchers generally connect to scc4.bu.edu :: 

    ssh username@scc4.bu.edu

``scc4.bu.edu`` is a restricted SCC head node that is only accessible from within the BU campus network. When working off campus, you may log into ``scc1.bu.edu`` first, which is an unrestricted head node that is accessible from anywhere, and then connect to ``scc4``. Alternatively, you may connect using a VPN; however, in practice, we've noticed that connecting to ``scc1`` provides a more consistent connection.
	
Transferring files to SCC
++++++++++++++++++++++++++++++++++++++++++++++++++++++
To transfer files from your local computer to SCC you can use any file transfer application that supports the `Secure Copy Protocol (SCP)` or the `Secure File Transfer Protocol (SFTP).`    
   
Examples of these are WinSCP for Windows, Fetch for Mac OS X, and the scp command for Linux.	
	
	
Software and Application Environments
++++++++++++++++++++++++++++++++++++++++++++++++++++++
Most applications on the SCC require the use of modules. Modules set the appropriate environment variables for the application and are useful in preventing conflicts related to different versions of an application. 

To avoid compatibility issues and for other technical reasons, the versions available by default for packages like MATLAB, R, and Python are very old. The ``module`` system may be used to access more recent versions of the applications.

Modules may be loaded and unloaded as needed. This is done using the ``module`` package, which offers several commands, including:

============================ ================================================================================================
Command                       Description                                                                                  
============================ ================================================================================================ 
 module list                  List currently loaded modules.                                                              
 module avail                 List modules that are available                                                             
 module help [modulefile]     Provides the description of the specified module  
 module load [modulefile]     Loads module. Or, specifies other packages that are required, but that have not been loaded 
 module unload [modulefile]   Unloads specified module from environment.                                                  
============================ ================================================================================================

.. attention::

    You may load a package by using the name of the application. This will load the version of the application that is available by default on SCC right now. The default version will change over time, as new versions of the application are installed on SCC. Therefore, it is recommended that you load modules using the full version. This will ensure that, in the future, your script works as expected, even when new versions of the software become available.   
    For example, to load samtools, you may simply do ::
    
        $ module load samtools 
    
    As of the time of this workshop, this loads version 1.5. Currently on SCC, the following options are available 
    
    samtools   
    samtools/1.1   
    samtools/1.3   
    samtools/1.4.1   
    samtools/samtools-0.1.18_gnu446   
    samtools/0.1.19   
    samtools/1.2   
    samtools/1.4   
    samtools/1.5   
    samtools/samtools-0.1.19_gnu446   
    
    As you can see, this package has been updated many times, and commands from newer versions might not be compatible with your scripts. Therefore, it is recommended that, instead, you load this package using ::
    
        $ module load samtools/1.5 
    
Modules may be loaded/unloaded interactively, and also from batch jobs (qsub files). It is recommended that when running batch jobs, you load modules from your qsub scripts. This will ensure that the job environment is consistent, and it will create a record of the software used for a particular analysis, which is critical for replicating the work. For example::

    #!/bin/bash -l
    
    #$ -P project
    #$ -cwd
    #$ -o basic.stdout
    #$ -e basic.stderr
    
    module load samtools/1.5
    
    #sort a BAM file and convert it to CRAM format
    samtools sort -O bam -T /tmp sample.bam | samtools view -T reference.fa -C -o sample.sorted.cram
    


When loading modules within a script, it is recommended to add the -l option to the first line of the script::

    #!/bin/bash -l 

For more information on module use on SCC, visit `this page <http://www.bu.edu/tech/support/research/software-and-programming/software-and-applications/modules/>`_



File Storage on SCC
++++++++++++++++++++++++++++++++++++++++++++++++++++++

There are several types of file storage available to SCC users. Below is a description of some of the major types:

* **Home Directory:** It is usually created for each user. By default, files stored here are visible and accessible only to the owner. The size is limited (10 GB), and may be used for long-term storage. This space is recommended for personal files, and it is backed up nightly. 

* **Backed-Up Project Disk Space:** It is assigned to each project, and it is shared by all the users assigned to the project. The size is moderate, and it is recommended for moderate to long-term storage. This space is recommended for files that are not replaceable or reproducible. This space is available at::

    /restricted/project/project_name/


* **Non-Backed Up Project Disk Space:** It is assigned to each project, and it is shared by all the users assigned to the project. The size is can be increased, and it is recommended for moderate to long-term storage. This space is recommended for large files, and files that can be downloaded, or reproduced. It is backed up (despite its name). This space is available at:: 

    /restricted/projectnb/project_name/


* **Scratch:** This space is available for each node on the SCC. It is intended for high-performance access during job execution. This space is shared by all users, and files should be removed once they job finishes running. Files are automatically removed after 30 days. Therefore, it only provides short-term storage. You may access this space from your qsub script by using:: 

    /scratch

* **/restricted partitions:** The backed-up project space and the non-backed up project space also exist under a ``/restricted`` partition. These partitions are used for storing data with dbGaP protection requirements (primarily Genomics projects), and are found under::

    /restricted/project 
    /restricted/projectnb partitions

For more details regarding file storage on SCC, visit this page on `Managing your Files <http://www.bu.edu/tech/support/research/system-usage/using-scc/managing-files/>`_
  
  