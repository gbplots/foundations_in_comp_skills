sample_names = expand('DC-0517-{sample}',sample=('2AL','2AH','6AL','6AH'))

rule all:
  input:
    expand('{sample}.10M_deduped_5p_scores.csv',sample=sample_names),
    'concat_counts_scores.10M_deduped_5p_scores.csv',
    'counts_dist.png',
    'scores_dist.png',
    'l_vs_h_filtered_sites.png'

rule download_rrna_fasta:
  output:
    'zebrRNA.fa',
    'zebrRNA.fa.sizes'
  shell:
    'wget https://ndownloader.figshare.com/articles/5298121/versions/2 -O tmp.zip && '
    'unzip tmp.zip && rm tmp.zip'

rule download_deduped_fasta:
  output:
    'DC-0517-2AH.10M_deduped.fasta.gz',
    'DC-0517-2AL.10M_deduped.fasta.gz',
    'DC-0517-6AH.10M_deduped.fasta.gz',
    'DC-0517-6AL.10M_deduped.fasta.gz'
  shell:
    'wget https://ndownloader.figshare.com/files/9071635?private_link=455fcc63e1b3c8ffa0b3 -O tmp_seq.zip && '
    'unzip tmp_seq.zip && rm tmp_seq.zip'

#rule deduplicate:
#  input: '{path}.fastq.gz'
#  output: '{path}_deduped.fasta.gz'
#  shell:
#    'python content/workshop/02_seq_process_app/process_reads.py {input}'

rule make_index:
  input: 'zebrRNA.fa'
  output: 'zebrRNA.fa.amb'
  shell:
    'bwa index {input}'

#rule make_index_sizes:
#  input: 'zebrRNA.fa'
#  output: 'zebrRNA.fa.sizes'
#  run:
#    with open(input[0]) as f :
#      with open(output[0],'w') as out_f :
#        for header in f :
#          seq = next(f).strip()
#          out_f.write('{}\t{}\n'.format(header[1:].strip(),len(seq)))

rule align_sort:
  input:
    fasta='{path}.fasta.gz',
    index='zebrRNA.fa.amb'
  output: '{path}.bam'
  params: prefix='zebrRNA.fa'
  shell:
    'bwa mem {params.prefix} {input.fasta} | '
    'samtools view -b | samtools sort > {output}'

rule genomecov:
  input:
    bam='{path}.bam',
    sizes='zebrRNA.fa.sizes'
  output: '{path}_5p.tsv'
  shell:
    'bedtools genomecov -d -5 -ibam {input.bam} -g {input.sizes} > {output}'

rule scores:
  input: '{path}.tsv'
  output: '{path}_scores.csv'
  run:
    from collections import namedtuple
    import csv
    PosCount = namedtuple('PosCount',['chrm','pos','value'])
    w = 10
    with open(input[0]) as f :
       f = csv.reader(f,delimiter='\t')
       with open(output[0],'w') as out_f :
         out_f = csv.writer(out_f,delimiter=',')
         counts = [PosCount(_[0],_[1],int(_[-1])) for _ in f]
         for i in range(w,len(counts)-w-1) :
           w_b = sum([_.value for _ in counts[i-w:i]])/w
           w_a = sum([_.value for _ in counts[i+1:i+w+1]])/w
           if w_b + w_a == 0 :
             score = 0
           else :
             score = counts[i].value / (0.5*w_b + 0.5*w_a)
           out_f.writerow((counts[i].chrm,counts[i].pos,score))

rule concat_scores:
  input:
    expand('{sample}.10M_deduped_5p.tsv',sample=sample_names)+
    expand('{sample}.10M_deduped_5p_scores.csv',sample=sample_names)
  output: 'concat_counts_scores.10M_deduped_5p_scores.csv'
  params:
    col_names=expand('{sample}_counts',sample=sample_names)+
              expand('{sample}_scores',sample=sample_names)
  run:
    import pandas

    all_counts_df = None
    for col_name, fn in zip(params.col_names,input):
      counts_df = pandas.read_table(fn
        ,names=('chrm','pos',col_name)
        ,sep=None
        ,engine='python'
      )
      counts_df.index = ['{}:{}'.format(_[0],_[1]) for i,_ in counts_df.iterrows()]
      counts_df.drop(['chrm','pos'],axis=1,inplace=True)
      if all_counts_df is None:
        all_counts_df = counts_df
      else :
        all_counts_df = all_counts_df.join(counts_df)
    print(all_counts_df.describe())
    all_counts_df.to_csv(output[0],index_label='pos')


rule make_plots:
  input: rules.concat_scores.output
  output:
    counts='counts_dist.png',
    scores='scores_dist.png',
    sites='l_vs_h_filtered_sites.png'
  run:
    import matplotlib
    matplotlib.use('agg')
    from matplotlib.pylab import figure
    import pandas

    all_df = pandas.read_csv(input[0],index_col=0)

    # counts
    counts_df = all_df[[_ for _ in all_df.columns if _.endswith('_counts')]]
    f = figure()
    counts_df.boxplot(ax=f.gca())
    f.gca().set_title('Counts distribution')
    f.savefig(output.counts)

    # scores
    scores_df = all_df[[_ for _ in all_df.columns if _.endswith('_scores')]]
    f = figure()
    scores_df.boxplot(ax=f.gca())
    f.gca().set_title('Scores distribution')
    f.savefig(output.scores)
   
    # filterd scores
    f = figure()
    ax = f.gca()
    filtered_scores_df_2h = scores_df[
      (scores_df['DC-0517-2AL_scores']>2) &
      (scores_df['DC-0517-2AH_scores']>0) &
      (scores_df['DC-0517-2AL_scores']>scores_df['DC-0517-2AH_scores'])
    ]
    ax.scatter(
      filtered_scores_df_2h['DC-0517-2AL_scores'],
      filtered_scores_df_2h['DC-0517-2AH_scores'],
      label='2H',
      alpha=0.2
    )

    filtered_scores_df_6h = scores_df[
      (scores_df['DC-0517-6AL_scores']>2) &
      (scores_df['DC-0517-6AH_scores']>0) &
      (scores_df['DC-0517-6AL_scores']>scores_df['DC-0517-6AH_scores'])
    ]
    ax.scatter(
      filtered_scores_df_6h['DC-0517-6AL_scores'],
      filtered_scores_df_6h['DC-0517-6AH_scores'],
      label='6H',
      alpha=0.2
    )

    ax.legend()
    f.savefig(output.sites)
