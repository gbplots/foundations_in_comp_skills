from collections import namedtuple, Counter
import csv
import gzip
from pprint import pprint
import sys

fn = sys.argv[1]

FastaRead = namedtuple('FastaRead',['header','seq'])
FastqRead = namedtuple('FastqRead',['header','seq','qual_header','qual'])
def fastq_itr(f) :
  for h in f :
    h = h.decode().strip()
    seq, _, qual = [next(f).decode().strip() for _ in range(3)]
    yield FastqRead(h,seq,_,qual)

counts = Counter()

deduped_seqs = set()

with gzip.open(fn) as f :
  for read in fastq_itr(f) :
    counts['total'] += 1
    if read.seq.endswith('CACA') :
      counts['CACA'] += 1
      deduped_seqs.add(read.seq)

counts['deduped'] = len(deduped_seqs)

fasta_fn = fn.replace('.fastq.gz','_deduped.fasta.gz')
unique_seqs = Counter()
with gzip.open(fasta_fn,'wb') as f :
  for i,seq in enumerate(deduped_seqs) :
    header = '>{}:{}\n'.format(fn,i)
    f.write(header.encode('utf8'))
    # trim off the random sequences
    seq = seq[4:-6]
    unique_seqs[seq] += 1
    seq = (seq+'\n').encode('utf8')
    f.write(seq)

counts['unique_seqs'] = len(unique_seqs)

stats_fn = fn.replace('.fastq.gz','_stats.csv')
with open(stats_fn,'w') as f :
  out_f = csv.writer(f)
  out_f.writerow(('stat','counts'))
  for stat, val in counts.items() :
    out_f.writerow((stat,val))

count_freq_fn = fn.replace('.fastq.gz','_seqs.txt')
with open(count_freq_fn,'w') as f :
  out_f = csv.writer(f)
  out_f.writerow(('sequence','count'))
  for seq, count in unique_seqs.most_common() :
    out_f.writerow((seq,count))
