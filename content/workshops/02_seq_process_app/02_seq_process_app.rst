Workshop 2. High Throughput Sequencing Application Session Online Materials
===========================================================================

Introduction
------------

.. youtube:: 67odXeyn27M

Illumina Sequencing Technology
------------------------------

An animation created by Illumina describing their sequencing by synthesis
technology.

.. youtube:: fCd6B5HRaZ8

High Throughput Sequencing Data Primer
--------------------------------------

Embarrasingly brief description of the form of high-throughput sequencing data
covering:

- FASTQ format
- PHRED quality scores
- single-end vs paired-end datasets
- dataset read length

.. youtube:: WLnYTiHwWAA

Sequencing Data QC and Analysis Primer
--------------------------------------

An even more embarrassingly brief description of the high level quality control
and analysis process for FASTQ formatted high-throughput datasets, including:

- typical read quality control metrics and operations
- extremely brief read mapping discussion

.. youtube:: IRWQPSKr8B8

Problem Description
-------------------

.. youtube:: WAVE038OOmQ

:doc:`02_seq_process_app_workshop`