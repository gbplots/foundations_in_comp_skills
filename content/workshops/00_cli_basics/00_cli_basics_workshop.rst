Workshop 0. Basic Linux and Command Line Usage: Workshop
================================================================

terminal_quest
--------------

In this workshop your only objective is to complete the `terminal_quest`_. You
can follow the instructions on the bitbucket page linked above to get started.
Good luck.

.. _terminal_quest: https://bitbucket.org/bubioinformaticshub/terminal_quest