GENCODE=ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_26/gencode.v26.annotation.gtf.gz

# goal 1
curl -s $GENCODE | zcat | grep -o 'gene_type "[^"]*";' | sort | uniq 

# goal II
curl -s $GENCODE | zcat | grep 'gene_type "rRNA";' | grep -o 'gene_name "[^"]*";' | sort | uniq | wc -l

# goal III
curl -s $GENCODE | zcat | grep 'gene_type "rRNA";' | cut -f 1,3,4,5 | grep 'gene' | wc -l

# goal IV
RESTURL=https://rest.ensembl.org/sequence/id
FASTA=gencode_v26_rrna.fasta
rm $FASTA
#curl -s $GENCODE | zcat | grep 'gene_type "rRNA";' | grep '\<gene\>' | \
#   grep -o 'ENSG...........' | head | fim - -e \
#   "curl $RESTURL/{}?type=genomic -H 'Content-type:text/x-fasta' >> $FASTA"
   
   
curl -s $GENCODE | zcat | grep 'gene_type "rRNA";' | grep '\<gene\>' | \
   grep -o 'ENSG...........' | head | xargs -n 1 -I{} \
   curl $RESTURL/{}?type=genomic -H 'Content-type:text/x-fasta' > $FASTA
