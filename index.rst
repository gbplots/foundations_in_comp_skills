.. Foundations in Computational Skills documentation master file, created by
      sphinx-quickstart on Mon Jul 10 16:33:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Foundations in Computational Skills's documentation!
===============================================================

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Contents:

   Workshop 0 - CLI <content/workshops/00_cli_basics/00_cli_basics>
   Workshop 1 - python <content/workshops/01_python/01_python_script>
   Workshop 2 - NGS 1 <content/workshops/02_seq_process_app/02_seq_process_app_workshop>
   Workshop 3 - Advanced CLI <content/workshops/03_advanced_cli/03_advanced_cli>
   Workshop 4 - Introduction to R <content/workshops/04_R/04_R>
   Workshop 5 - NGS 2 <content/workshops/05_ngs_app_session_2/05_ngs_app_session_2>
   Workshop 6 - Cluster Computing <content/workshops/06_cluster_computing/06_cluster_computing>

Contents
--------

- `Workshop 0 - CLI Basics`_
- `Workshop 1 - python`_
- `Workshop 2 - High throughput sequencing application`_
- `Workshop 3 - Advanced CLI`_
- `Workshop 4 - Introduction to R`_
- `Workshop 5 - High throughput sequencing application part 2`_
- `Workshop 6 - Shell scripting and cluster computing`_

Friendly Introduction
---------------------

.. youtube:: OQzcX019Vmc

List of topics
--------------

.. note:: These workshops are still under construction
   Please be kind.

Workshop 0 - CLI Basics
+++++++++++++++++++++++

:doc:`content/workshops/00_cli_basics/00_cli_basics`

======================================== =======
Topic                                    Length
======================================== =======
`CLI Introduction`_                      3 min
`Navigating directories, listing files`_ 16 min
`Basic file operations`_                 9 min
`Working with files 1`_                  10 min
`Working with files 2`_                  10 min
`I/O redirection and related commands`_  20 min
`Globbing`_                              6 min
======================================== =======

.. _`CLI Introduction`: http://www.youtube.com/watch?v=M5I6pNxVln4
.. _`Navigating directories, listing files`: https://www.youtube.com/watch?v=MmHcOPJEjGA
.. _`Basic file operations`: https://www.youtube.com/watch?v=qG8qn4ZARvg
.. _`Working with files 1`: https://www.youtube.com/watch?v=hNb8gIHvN04
.. _`Working with files 2`: https://www.youtube.com/watch?v=MJI4xTuxdPg
.. _`I/O redirection and related commands`: https://www.youtube.com/watch?v=WXcvQ5F7Kh4
.. _`Globbing`: https://www.youtube.com/watch?v=6iQEKElUoI8

Workshop 1 - python
+++++++++++++++++++

:doc:`content/workshops/01_python/01_python_script`

:doc:`content/workshops/01_python/01_python_workshop`

======================================================== =======
Topic                                                    Length
======================================================== =======
Introduction                                             3 min
Builtin Types                                            2 min
A Simple Program                                         6 min
if Statements                                            3 min
Simplifying Solution                                     4 min
Defining Functions                                       4 min
More On Types and Iteration                              4 min
More Data Structures                                     -
File I/O                                                 22 min
Modules                                                  3 min
Executing Scripts with Arguments from the Command Line   3 min
Other File objects                                       -
======================================================== =======

Workshop 2 - High throughput sequencing application
+++++++++++++++++++++++++++++++++++++++++++++++++++

:doc:`content/workshops/02_seq_process_app/02_seq_process_app`

:doc:`content/workshops/02_seq_process_app/02_seq_process_app_workshop`

========================================= =======
Topic                                     Length
========================================= =======
`App Session Introduction`_               2 min
`Illumina Sequencing Technology`_         5 min
`High Throughput Sequencing Data Primer`_ 9 min
`Sequencing Data QC and Analysis Primer`_ 9 min
========================================= =======

.. _`App Session Introduction`: http://www.youtube.com/watch?v=67odXeyn27M
.. _`Illumina Sequencing Technology`: https://www.youtube.com/watch?v=fCd6B5HRaZ8
.. _`High Throughput Sequencing Data Primer`: http://www.youtube.com/watch?v=WLnYTiHwWAA
.. _`Sequencing Data QC and Analysis Primer`: http://www.youtube.com

Workshop 3 - Advanced CLI
+++++++++++++++++++++++++

:doc:`content/workshops/03_advanced_cli/03_advanced_cli`

:doc:`content/workshops/03_advanced_cli/03_advanced_cli_workshop`

========================================= ==============
Topic                                     Length
========================================= ==============
`nano`_                                   ~7 min
vim (part `one`_, `two`_, `three`_)       ~9, ~6, ~6 min
`emacs`_                                  ~24 min
`piping, silencing stdout/stderr`_        ~10 min
`shell tricks`_                           ~10 min
`bash history`_                           ~4 min
`pushd/popd, find, xargs/fim`_            ~10 min
========================================= ==============

.. _nano: http://www.youtube.com/watch?v=cLyUZAabf40
.. _one: http://www.youtube.com/watch?v=c6WCm6z5msk
.. _two: http://www.youtube.com/watch?v=BPDoI7gflxM
.. _three: http://www.youtube.com/watch?v=J1_CfIb-3X4
.. _emacs: http://www.youtube.com/watch?v=16Rd46SE-20
.. _piping, silencing stdout/stderr: http://www.youtube.com/watch?v=vLJOmO1WYL4
.. _shell tricks: http://www.youtube.com/watch?v=7lSdSbgvAvs
.. _bash history: http://www.youtube.com/watch?v=WG-MGFPsLhk
.. _pushd/popd, find, xargs/fim: http://www.youtube.com/watch?v=k_Qt2khwA7c

Workshop 4 - Introduction to R
++++++++++++++++++++++++++++++++++++

:doc:`content/workshops/04_R/04_R`

:doc:`content/workshops/04_R/R_04_workshop`

========================================= ==============
Topic                                     Length
========================================= ==============
`Reasons why you should learn R`_         ~7 min
`RStudio interface`_                      ~10 min
`Installing packages`_                    ~7 min
`Console and working environment basics`_ ~8 min
`Very quick data types`_                  ~7 min
`NAs, NaNs, Infs`_                        ~3 min
`Intro to vectors`_                       ~7 min
`Data types and structures, Part 2`_      9:30 min
`If statements and logical operators`_    5:30 min
`Apply function`_                         ~7 min
`Reshape package`_                        ~6 min
`Split, Apply and Combine with plyr`_     8:30 min
`Intro to ggplot`_                        9 min 
========================================= ==============

.. _Reasons why you should learn R: http://www.youtube.com/watch?v=G1PLkaRL2iY
.. _RStudio interface: http://www.youtube.com/watch?v=jAgbZ8jkBtQ
.. _Installing packages: http://www.youtube.com/watch?v=0cCuHhfphtQ
.. _Console and working environment basics: http://www.youtube.com/watch?v=v5g_JZK_Ggs
.. _Very quick data types: http://www.youtube.com/watch?v=B2f9tSGVn7w
.. _NAs, NaNs, Infs: http://www.youtube.com/watch?v=ofe2hDUg78g
.. _Intro to vectors: http://www.youtube.com/watch?v=uUxwf1lWgrk
.. _Data types and structures, Part 2: http://www.youtube.com/watch?v=_HKDbA9WkX8
.. _If statements and logical operators: http://www.youtube.com/watch?v=eVEx_pBEkRI
.. _Apply function: http://www.youtube.com/watch?v=csLati8vpOo
.. _Reshape package: http://www.youtube.com/watch?v=aXXy04P_l1c
.. _Split, Apply and Combine with plyr: http://www.youtube.com/watch?v=S4oCN0FDC14
.. _Intro to ggplot: http://www.youtube.com/watch?v=O9tudYwK1RM

Workshop 5 - High throughput sequencing application part 2
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

:doc:`content/workshops/05_ngs_app_session_2/05_ngs_app_session_2`

:doc:`content/workshops/05_ngs_app_session_2/05_ngs_app_session_2_workshop`

There are no video online materials for this workshop.


Workshop 6 - Shell scripting and cluster computing
++++++++++++++++++++++++++++++++++++++++++++++++++

:doc:`content/workshops/06_cluster_computing/06_cluster_computing`

------------

Contributors
------------

The primary contributors to the BU Bioinformatics Programming Workshop Taskforce:

- Gracia Bonilla
- Rachael Ivison
- Vinay Kartha
- Josh Klein
- Adam Labadorf
- Katharine Norwood

We would also like to thank Gary Benson for his mentorship and support.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`